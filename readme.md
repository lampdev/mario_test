# Affiliate Form


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Environment Requirements

The next requirements should be satisfied by your system:
Composer Requirements [Link](https://getcomposer.org/doc/00-intro.md#system-requirements)
Laravel 5.3 Requirements [Link](https://laravel.com/docs/5.3#server-requirements)

### Installing

Setup your web server host directory to `{project_dir}/public`

Composer install command from `{project_dir}` directory

```
composer install
```

Make your `.env` file by example from `.env.example` file

Generate Laravel App Key
```
php artisan key:generate
```
Make Laravel migration

```
 php artisan migrate
```

Done.
You can check the project via browser.

## Built With

* [Laravel](http://www.dropwizard.io/1.0.2/docs/) - The web framework used

## Authors

* **LampDev** - [our web-site](https://lamp-dev.com/)
