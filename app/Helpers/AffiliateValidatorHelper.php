<?php

namespace App\Helpers;

class AffiliateValidatorHelper
{
    private $allItems;

    public function __construct()
    {
        $this->allItems = [
            'affiliate_name' => 'string|required',
            'email' => 'email|required',
            'title' => 'string|required',
            'slug' => 'string|required',
            'content' => 'string|required',
            'img' => 'image|required'
        ];
    }

    public function getIndexFields()
    {
        return $this->getItems([
            'affiliate_name',
            'email',
            'title',
            'slug',
            'content'
        ]);
    }

    public function getItems(array $itemsNames)
    {
        $items = [];
        foreach ($itemsNames as $itemsName) {
            if (isset($this->allItems[$itemsName])) {
                $items[$itemsName] = $this->allItems[$itemsName];
            }
        }
        return $items;
    }
}
