<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateImagesModel extends Model
{
    protected $table = 'affiliates_images';

    protected $fillable = [
        'affiliate_id',
        'image'
    ];
}
