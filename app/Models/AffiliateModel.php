<?php

namespace App\Models;

use App\Models\AffiliateImagesModel;
use Illuminate\Database\Eloquent\Model;

class AffiliateModel extends Model
{
    protected $table = 'affiliates';

    protected $fillable = [
        'affiliate_name',
        'email',
        'title',
        'slug',
        'content',
        'views'
    ];

    public function modelGetById(int $id)
    {
        return static::find($id);
    }

    public function modelGetBySlug(string $slug)
    {
        return static::where('slug', $slug)->first();
    }

    public function getLocalHostedImages($id)
    {
        $records = (
            $this->images()
                ->where('affiliate_id', $id)
                ->get()
        );

        return $records;
    }

    public function images()
    {
        return $this->hasMany('App\Models\AffiliateImagesModel', 'affiliate_id', 'id');
    }
}
