<?php

namespace App\Http\Controllers;

use App\Helpers\AffiliateValidatorHelper;
use App\Models\AffiliateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Storage;

class AffiliateController extends Controller
{
    private $validationHelper;
    private $affiliateModel;

    public function __construct(
        AffiliateValidatorHelper $validationHelper,
        AffiliateModel $affiliateModel
    ) {
        $this->validationHelper = $validationHelper;
        $this->affiliateModel = $affiliateModel;
    }

    public function index()
    {
        return view('pages.affiliateForm');
    }

    public function save(Request $request)
    {
        $info = $request->only([
            'affiliate_name',
            'email',
            'title',
            'slug',
            'content'
        ]);

        $images = $request->file('img');

        if (empty($images)) {
            return (
                Redirect::to('/')
                    ->withErrors($validator = "The images field is required.")
            );
        }

        $validator = Validator::make(
            $info,
            $this->validationHelper->getIndexFields()
        );

        if ($validator->fails()) {
            return (
                Redirect::to('/')
                    ->withErrors($validator)
            );
        }

        $affiliate = $this->affiliateModel->create($info);
        if (empty($affiliate->id)) {
            return null;
        }

        foreach ($images as $image) {
            $this->saveImage($image, $affiliate->id);
        }

        $slug =  str_replace(' ', '-', $info['slug']);

        return (
            Redirect::to($slug)
        );
    }

    public function saveImage(
        $img,
        int $id
    ) {
        if (empty($id)) {
            return null;
        }

        $affiliate = $this->affiliateModel->modelGetById($id);
        if (empty($affiliate)) {
            return null;
        }

        $fileName = Storage::putFile('', $img);
        $fileDbData = [
            'image' => $fileName
        ];
        $affiliate->images()->create($fileDbData);

        return $fileName;
    }

    public function get($slug)
    {
        $slug =  str_replace('-', ' ', $slug);
        $affiliate = $this->affiliateModel->modelGetBySlug($slug);

        $affiliate->views += 1;
        $affiliate->save();

        return view('pages.affiliateImages', ['affiliate' => $affiliate]);
    }
}
