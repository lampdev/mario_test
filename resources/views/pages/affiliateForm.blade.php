@extends('layouts.main')

@section('content')
<div class="container">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <div class="form-area">
            <form enctype="multipart/form-data" role="form" action="/" method="POST">
                {!! csrf_field() !!}
                <br style="clear:both">
                <h3 style="margin-bottom: 25px; text-align: center;">Affiliate Form</h3>
                <div class="form-group">
                    <input type="text" class="form-control" id="affiliate_name" name="affiliate_name" placeholder="Affiliate Name">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug">
                </div>
                <div class="form-group">
                    <textarea class="form-control" type="textarea" id="content" name="content" placeholder="Content" maxlength="250" rows="7"></textarea>
                </div>
                <div class="form-group">
                     <p>Select images: </p><input type="file" name="img[]" multiple>
                </div>
                <button type="submit" id="submit" class="btn btn-primary pull-right">
                    Submit Form
                </button>
            </form>
        </div>
    </div>
</div>

@stop
