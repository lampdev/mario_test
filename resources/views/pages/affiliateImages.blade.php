@extends('layouts.main')

@section('content')
 <div class="container">
        <div class="row">
            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="text-center">
                    <h1 class="gallery-title">Affiliate</h1>
                </div>
            </div>

            @foreach($affiliate->images->toArray() as $image)
                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                    <img src="{{ URL::asset(Storage::url($image['image'])) }}" class="img-responsive">
                </div>
            @endforeach

            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Affiliate Name</th>
                  <th>Email</th>
                  <th>Title</th>
                  <th>Slug</th>
                  <th>Content</th>
                  <th>Views</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $affiliate->affiliate_name }}</td>
                  <td>{{ $affiliate->email }}</td>
                  <td>{{ $affiliate->title }}</td>
                  <td>{{ $affiliate->slug }}</td>
                  <td>{{ $affiliate->content }}</td>
                  <td>{{ $affiliate->views }}</td>
                </tr>
              </tbody>
            </table>
        </div>
</section>

@stop
