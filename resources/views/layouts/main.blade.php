<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')

        @include('includes.scripts')
    </head>
    <body>
        @if (count($errors) > 0)
            <div class = "alert alert-danger">
                <ul>
                   @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                   @endforeach
                </ul>
            </div>
        @endif

        @yield('content')
    </body>
</html>
