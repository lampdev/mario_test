<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    [
        'as' => '/',
        'uses' => 'AffiliateController@index'
    ]
);

Route::post(
    '/',
    [
        'as' => '/',
        'uses' => 'AffiliateController@save'
    ]
);

Route::get(
    '{slug}',
    [
        'as' => '/',
        'uses' => 'AffiliateController@get'
    ]
);

Route::get('test', function () {
    return view('pages.affiliateImages');
});

Route::get('storage/{filename}', function ($filename) {
    $path = storage_path() . '/app/' . $filename;

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
